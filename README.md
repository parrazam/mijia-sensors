# Bluetooth LE to MQTT bridge for the Xiaomi Mijia Temperature & Humidity sensor

[![Build Status](https://ci.cuzo.dev/api/badges/parra/mijia-sensors/status.svg)](https://ci.cuzo.dev/parra/mijia-sensors)

## Create environment file

Copy the `env_file` to `.env` and open it. Complete all environment variables:

- `MQTT_[SERVER/PORT/USER/PASSWORD/CLIENT_ID]` are used to connect with the broker.
- `MQTT_TOPIC_PREFIX` define the prefix for all topics with sensor info.
- `MQTT_TELE_PREFIX` is used to publish the sensor data, like battery or status.
- `MQTT_SENSOR_NAME` contains the sensor name, useful to split the telemetry data if you have more than one sensors.
- `MQTT_PUBLISH_DELAY` specify, in seconds, how many time should wait since the script take the measurements to publish in the broker
- `MIJIA_BTLE_ADDRESS` constant with the BLE address of your Mijia device.. This can be retrieved activating the pairing mode in the sensor and scanning the BT devices
- `PING_URL` used to ping a monitoring service if everything was OK (Useful, for example, for monitor the script with Uptime Kuma)

## Install dependencies

You'll need to install bluez and python3. Then you'll need pip3 to install bluepy.

Example on a Raspberry Pi 3:
```sh
$ sudo apt-get install python-pip libglib2.0-dev
$ sudo pip3 install -r requirements.txt
```

## Run

You can execute the script directly using the command:
```sh
$ ./main.py
```

Or you can add a new entry in the `crontab`, like:
```sh
*/20 * * * * /usr/bin/python3 ~/scripts/mijia-temperature/main.py >~/scripts/mijia-temperature/last.log 2>&1
```

The published message will have this structure:
```json
{
  "id": "mijia-sensor-1", 
  "battery": 74, 
  "temperature": 21.4, 
  "humidity": 47.3
}
```
